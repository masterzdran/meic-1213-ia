sublista(Su,L,S1,S2):- juntar(S1,Su,S3),juntar(S3,S2,L).
sublista1(L1,L2):-juntar(K,W,L2),juntar(L1,V,W).


flat([],[]).
flat([A|R],[A|R1]):-atom(A),!,flat(R,R1).
flat([A|R],B):-flat(A,A1),flat(R,R1),juntar(A1,R1,B).

flat1([],[]).
flat1([X|L],K):-flat1(X,K1),!,flat1(L,K2),juntar(K1,K2,K).
flat1([X|L],[X|K]):-flat1(L,K).

ordenar([],[]).
ordenar([H|T], K):- menores(H,T,Me), maiores(H,T,Ma), ordenar(Me,Me1), ordenar(Ma, Ma1), juntar(Me1,[H],Aux), juntar(Aux,Ma1,K).

menores(_,[],[]).
menores(P,[H|T],[H|T1]):- H<P,!, menores(P,T,T1).
menores(P,[H|T],T1):-menores(P,T,T1).

maiores(_,[],[]).
maiores(P,[H|T],[H|T1]):- H>P,!, maiores(P,T,T1).
maiores(P,[H|T],T1):-maiores(P,T,T1).

juntar([],X,X).
juntar([A|R],X,[A|R1]):-juntar(R,X,R1).


inserir(E,L,[E|L]).
inserir(E,[E1|R],[E1|R1]):-inserir(E,R,R1).

permutacao([],[]).
permutacao([E|R],P):-permutacao(R,R1),inserir(E,R1,P).

estaOrdenado([]).
estaOrdenado([E]).
estaOrdenado([E1,E2|R]):-E1<E2,estaOrdenado([E2|R]).

ordenargT(L,O):-permutacao(L,O),estaOrdenado(O).